using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class List : MonoBehaviour
{
    bool canSell = true;
    public PotionMake PM;
    public GameObject pot1, pot2;
    [SerializeField] GameObject spawnPoint;
    [SerializeField] GameObject canvas;
    SpriteRenderer PotColor;
    [SerializeField] TextMeshProUGUI colorText;
    [SerializeField] TextMeshProUGUI tasteText;
    [SerializeField] TextMeshProUGUI effectText;
    public int life = 0, goodPotion = 0;
    bool gameOver = false;
    bool gameWin = false;
    [SerializeField] GameObject goodPImage;
    [SerializeField] GameObject badPImage;
    [SerializeField] GameObject endScreen;
    [SerializeField] TextMeshProUGUI endText;
    [SerializeField] GameObject cursor;
    [SerializeField] TextMeshProUGUI lifeText;
    [SerializeField] TextMeshProUGUI PotionText;
    int PT;
    private void Start()
    {
        Color = Random.Range(1, 9);
        Taste = Random.Range(1, 9);
        Effect = Random.Range(1, 9);
        StartCoroutine(ShowOrderText());
    }
    private void Update()
    {
        if (goodPotion >= 10 && !gameWin) {
            Debug.Log("Wygra�e�");
            gameWin = true;
            endScreen.SetActive(true);
            Cursor.visible = true;
            endText.text = "You Won!";
            cursor.SetActive(false);
        }
        if (life <= 0 && !gameOver) {
            Debug.Log("Przegra�e�");
            gameOver = true;
            endScreen.SetActive(true);
            endText.text = "You Lose!";
            Cursor.visible = true;
            cursor.SetActive(false);
        }
    }
    public int Color, Taste, Effect;
    public void Order() {
        if (canSell) {
            StartCoroutine(SellOrder());
           
        }
    }
    IEnumerator SellOrder() {
        canSell = false;
        if (PM.Slot1 == Color)
        {
            if (PM.Slot2 == Taste)
            {
                if (PM.Slot3 == Effect)
                {
                    Debug.Log("Dobra Mikstura");
                    goodPotion ++;
                    PotionText.text = goodPotion.ToString();
                    StartCoroutine(ShowGoodPotion());
                }
                else
                {
                    Debug.Log("Z�a Mikstura");
                    life --;
                    lifeText.text = life.ToString();
                    StartCoroutine(ShowBadPotion());
                }
            }
            else
            {
                Debug.Log("Z�a Mikstura");
                life--;
                lifeText.text = life.ToString();
                StartCoroutine(ShowBadPotion());
            }
        }
        else {
            Debug.Log("Z�a Mikstura");
            life--;
            lifeText.text = life.ToString();
            StartCoroutine(ShowBadPotion());
        }
    

        GameObject mPref = Instantiate(pot1);
        mPref.transform.parent = canvas.transform;
        mPref.transform.position = new Vector2(spawnPoint.transform.position.x, spawnPoint.transform.position.y);

        GameObject mPref2 = Instantiate(pot2);
        PotColor = mPref2.gameObject.GetComponent<SpriteRenderer>();
        if (PM.Slot1 == 1) {
            PotColor.color = new Color(1, 0.5f, 0, 1);
        }
        else if (PM.Slot1 == 2)
        {
            PotColor.color = new Color(1, 0, 0, 1);
        }
        else if (PM.Slot1 == 3)
        {
            PotColor.color = new Color(1, 1, 1, 1);
        }
        else if (PM.Slot1 == 4)
        {
            PotColor.color = new Color(0, 0.3f, 1, 1);
        }
        else if (PM.Slot1 == 5)
        {
            PotColor.color = new Color(1, 1, 0, 1);
        }
        else if (PM.Slot1 == 6)
        {
            PotColor.color = new Color(1, 0, 1, 1);
        }
        else if (PM.Slot1 == 7)
        {
            PotColor.color = new Color(0.3f, 0.3f, 0, 1);
        }
        else if (PM.Slot1 == 8)
        {
            PotColor.color = new Color(0, 1, 0, 1);
        }
        else if (PM.Slot1 == 9)
        {
            PotColor.color = new Color(0, 0, 0, 0);
        }
        mPref2.transform.parent = canvas.transform;
        mPref2.transform.position = new Vector2(spawnPoint.transform.position.x, spawnPoint.transform.position.y);

        
        

        yield return new WaitForSeconds(3);
        PM.Slot1 = 0;
        PM.Slot2 = 0;
        PM.Slot3 = 0;
        Destroy(mPref);
        Destroy(mPref2);
        Color = Random.Range(1, 9);
        Taste = Random.Range(1, 9);
        Effect = Random.Range(1, 9);
        canSell = true;

        StartCoroutine(ShowOrderText());
    }
    IEnumerator ShowGoodPotion() 
    {
    goodPImage.SetActive(true);
        yield return new WaitForSeconds(2.5f);
        goodPImage.SetActive(false);
    }
    IEnumerator ShowBadPotion()
    {
        badPImage.SetActive(true);
        yield return new WaitForSeconds(2.5f);
        badPImage.SetActive(false);
    }
    IEnumerator ShowOrderText() {
        yield return new WaitForSeconds(0.25f);
        if (Color == 1)
        {
            colorText.text = "Orange";
        }
        else if (Color == 2)
        {
            colorText.text = "Red";
        }
        else if (Color == 3)
        {
            colorText.text = "White";
        }
        else if (Color == 4)
        {
            colorText.text = "Blue";
        }
        else if (Color == 5)
        {
            colorText.text = "Yellow";
        }
        else if (Color == 6)
        {
            colorText.text = "Purple";
        }
        else if (Color == 7)
        {
            colorText.text = "Brown";
        }
        else if (Color == 8)
        {
            colorText.text = "Green";
        }
        else if (Color == 9)
        {
            colorText.text = "Transparent";
        }

        if (Taste == 1)
        {
            tasteText.text = "Sweet";
        }
        else if (Taste == 2)
        {
            tasteText.text = "Meet";
        }
        else if (Taste == 3)
        {
            tasteText.text = "Snot";
        }
        else if (Taste == 4)
        {
            tasteText.text = "Blueberry";
        }
        else if (Taste == 5)
        {
            tasteText.text = "Candy";
        }
        else if (Taste == 6)
        {
            tasteText.text = "No taste";
        }
        else if (Taste == 7)
        {
            tasteText.text = "Bitter";
        }
        else if (Taste == 8)
        {
            tasteText.text = "France food";
        }
        else if (Taste == 9)
        {
            tasteText.text = "Potato";
        }
        if (Effect == 1)
        {
            effectText.text = "Poison";
        }
        else if (Effect == 2)
        {
            effectText.text = "No effect";
        }
        else if (Effect == 3)
        {
            effectText.text = "Dark vision";
        }
        else if (Effect == 4)
        {
            effectText.text = "Glow effect";
        }
        else if (Effect == 5)
        {
            effectText.text = "Healing";
        }
        else if (Effect == 6)
        {
            effectText.text = "Flying";
        }
        else if (Effect == 7)
        {
            effectText.text = "Antidote";
        }
        else if (Effect == 8)
        {
            effectText.text = "Respiration";
        }
        else if (Effect == 9)
        {
            effectText.text = "Cold syrupe";
        }
    }
}
