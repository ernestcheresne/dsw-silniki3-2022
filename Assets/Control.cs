using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Control : MonoBehaviour
{
    public void restartScene() 
    {
        SceneManager.LoadScene("Level_0");
    }
    public void goMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
    bool checkBook = false;
    [SerializeField] GameObject book;
    public void showBook() {
        if (checkBook) {
            checkBook = false;
            book.SetActive(false);
        }
        else{
            checkBook = true;
            book.SetActive(true);
        }
    }
    bool checkOrder = false;
    [SerializeField] Transform order;
    public void showOrder()
    {
        if (checkOrder)
        {
            checkOrder = false;
            order.localPosition = new Vector3(286, -110, 0);
            
        }
        else
        {
            checkOrder = true;
            order.localPosition = new Vector3(600, 600, 0);
        }
    }
}
