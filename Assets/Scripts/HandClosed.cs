using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HandClosed : MonoBehaviour
{
  public Sprite Closed;

  void OnMouseDown(){
    Debug.Log("Hand Closed");
      this.gameObject.GetComponent<SpriteRenderer>().sprite = Closed;
  }
}
