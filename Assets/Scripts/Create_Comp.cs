using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Create_Comp : MonoBehaviour
{
  [SerializeField] GameObject pref;
  [SerializeField] GameObject spawnPoint;
  [SerializeField] GameObject canvas;
   bool canCreate = true;
   public void Create(){
     if (canCreate ) {
       StartCoroutine(SpawnM());
     }

   }
    IEnumerator SpawnM(){
      canCreate = false;
      GameObject mPref = Instantiate(pref);
      mPref.transform.parent = canvas.transform;
      mPref.transform.position = new Vector2(spawnPoint.transform.position.x, spawnPoint.transform.position.y);
      mPref.transform.localScale = new Vector3(15,15,15);
      yield return new WaitForSeconds(1f);

      canCreate = true;
    }

}
