using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragObject : MonoBehaviour
{
    public bool isDraging;

  Vector3 mousePositionOffset;

  private Vector3 GetMouseWorldPosition(){
  return   Camera.main.ScreenToWorldPoint(Input.mousePosition);
  }

  private void OnMouseDown(){
    mousePositionOffset = gameObject.transform.position - GetMouseWorldPosition();
        isDraging = true;
    }
    private void OnMouseUp()
    {
        isDraging = false;
    }
    private void OnMouseDrag(){
    transform.position = GetMouseWorldPosition() + mousePositionOffset;
  }
}
