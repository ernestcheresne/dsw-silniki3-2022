using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PotionMake : MonoBehaviour
{
    public GameObject ingredient1;
    public GameObject ingredient2;
    public GameObject ingredient3;
    public DragObject DO;

    public int Slot1, Slot2, Slot3;
    bool canCheck = true;
    private void OnCollisionStay2D(Collision2D collision)
    {
        
        if (canCheck) {
            StartCoroutine(isIn(collision));
        }
    }
    IEnumerator isIn(Collision2D collision) {
        canCheck = false;
        DO = collision.gameObject.GetComponent<DragObject>();
        yield return new WaitForSeconds(0.25f);
        if (Slot1 == 0 && !DO.isDraging)
        {
            ingredient1 = collision.gameObject;
            if (collision.gameObject.CompareTag("Mushrooms"))
            {
                Slot1 = 1;
                
            }
            else if (collision.gameObject.CompareTag("Rat"))
            {
                Slot1 = 2;
            }
            else if (collision.gameObject.CompareTag("Aye"))
            {
                Slot1 = 3;
            }
            else if (collision.gameObject.CompareTag("GlowMushroom"))
            {
                Slot1 = 4;
            }
            else if (collision.gameObject.CompareTag("AmberBug"))
            {
                Slot1 = 5;
            }
            else if (collision.gameObject.CompareTag("ButterflyWings"))
            {
                Slot1 = 6;
            }
            else if (collision.gameObject.CompareTag("Cockroach"))
            {
                Slot1 = 7;
            }
            else if (collision.gameObject.CompareTag("Frog"))
            {
                Slot1 = 8;
            }
            else if (collision.gameObject.CompareTag("Mandragora"))
            {
                Slot1 = 9;
            }
            Destroy(collision.gameObject);
        }
        else if (Slot2 == 0 && !DO.isDraging)
        {
            ingredient2 = collision.gameObject;
            if (collision.gameObject.CompareTag("Mushrooms"))
            {
                Slot2 = 1;
            }
            else if (collision.gameObject.CompareTag("Rat"))
            {
                Slot2 = 2;
            }
            else if (collision.gameObject.CompareTag("Aye"))
            {
                Slot2 = 3;
            }
            else if (collision.gameObject.CompareTag("GlowMushroom"))
            {
                Slot2 = 4;
            }
            else if (collision.gameObject.CompareTag("AmberBug"))
            {
                Slot2 = 5;
            }
            else if (collision.gameObject.CompareTag("ButterflyWings"))
            {
                Slot2 = 6;
            }
            else if (collision.gameObject.CompareTag("Cockroach"))
            {
                Slot2 = 7;
            }
            else if (collision.gameObject.CompareTag("Frog"))
            {
                Slot2 = 8;
            }
            else if (collision.gameObject.CompareTag("Mandragora"))
            {
                Slot2 = 9;
            }
            Destroy(collision.gameObject);
        }
        else if (Slot3 == 0 && !DO.isDraging)
        {
            ingredient3 = collision.gameObject;
            if (collision.gameObject.CompareTag("Mushrooms"))
            {
                Slot3 = 1;
            }
            else if (collision.gameObject.CompareTag("Rat"))
            {
                Slot3 = 2;
            }
            else if (collision.gameObject.CompareTag("Aye"))
            {
                Slot3 = 3;
            }
            else if (collision.gameObject.CompareTag("GlowMushroom"))
            {
                Slot3 = 4;
            }
            else if (collision.gameObject.CompareTag("AmberBug"))
            {
                Slot3 = 5;
            }
            else if (collision.gameObject.CompareTag("ButterflyWings"))
            {
                Slot3 = 6;
            }
            else if (collision.gameObject.CompareTag("Cockroach"))
            {
                Slot3 = 7;
            }
            else if (collision.gameObject.CompareTag("Frog"))
            {
                Slot3 = 8;
            }
            else if (collision.gameObject.CompareTag("Mandragora"))
            {
                Slot3 = 9;
            }
            Destroy(collision.gameObject);
        }
        canCheck = true;
    }
    public void ResetTable()
    {
        Slot1 = 0;
        Slot2 = 0;
        Slot3 = 0;
        
    }
}
